package HomeWork6;

public class Main {


    public static void main(String[] args) {
        Pet dog = new Pet(Species.DOG, "jackie", 7, 75, new String[]{"eat", "drink", "sleep"});
        Pet cat = new Pet(Species.CAT, "jackie", 5, 95, new String[]{"eat", "drink", "sleep"});
        Pet fish = new Pet(Species.FISH, "jackie", 3, 75, new String[]{"eat", "drink", "sleep"});
        Pet rabbit = new Pet(Species.RABBIT, "jackie", 1, 75, new String[]{"eat", "drink", "sleep"});
        Pet bird = new Pet(Species.BIRD, "jackie", 2, 95, new String[]{"eat", "drink", "sleep"});
        Pet horse = new Pet(Species.HORSE, "jackie", 11, 85, new String[]{"eat", "drink", "sleep"});

        Human mother = new Human("jane ", "Karleon", 1989);
        Human father = new Human("john", "Karleon", 1967);
        Human child = new Human("Jack", "Karleon", 10, 98, mother, father, rabbit, new String[][]{new String[]{DayOfWeek.Sunday.name(), "Go for a walk"}});
        Human child1 = new Human("Adam", "Karleon", 17, 101, mother, father, rabbit, new String[][]{new String[]{DayOfWeek.Sunday.name(), "Go for a walk"}});
        Human child2 = new Human("Jack", "Karleon", 10, 98, mother, father, fish, new String[][]{new String[]{DayOfWeek.Sunday.name(), "Go for a walk"}});
        Family family = new Family(mother, father);
        family.addChild(child);
        family.addChild(child1);
        family.addChild(child2);
        System.out.println(family);
        family.deleteChild(child1);
        System.out.println(family.toString());


    }

}


